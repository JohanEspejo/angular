import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './destino-Viaje.models';



import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { getAllLifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';

@Injectable()
export class DestinosApiClient {

    destinos: DestinoViaje[] = [];

    constructor(){

    }

    

    add(d: DestinoViaje){

        this.destinos.push(new DestinoViaje(d.nombre,d.url));
        return false;

    }
    
    getAll(): DestinoViaje[] {
        return this.destinos;
    }
    
}


  
